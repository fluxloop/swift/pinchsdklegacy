// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "PinchSDKLegacy",
    products: [
        .library(
            name: "PinchSDKLegacy",
            targets: ["PinchSDKLegacy"]
        )
    ],
    dependencies: [

    ],
    targets: [
        .binaryTarget(
            name: "PinchSDKLegacy",
            url: "https://puresdk.blob.core.windows.net/pinchsdk-versions/xcf/PinchSDKLegacy.1.0.103.zip",
            checksum: "d3f79d08eaf7d8a8e219260d71c09d7d7004eb1c374d15cfae716d030dfdcd30"
            
        )
    ]
)
